A collection of one-off QA scripts I use(d) to fix issues in the Debian archive.

Useful when you don't have a lintian tag and thus can't use lintian-brush +
Janitor :)
