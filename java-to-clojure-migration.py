#!/bin/python3

# Copyright 2021, Louis-Philippe Véronneau <pollo@debian.org>
#
# This script is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This script is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this script. If not, see <http://www.gnu.org/licenses/>.

"""
This script is used to migrate packages from the Java Team to the Clojure Team.
"""

import json
import os
import re

from argparse import ArgumentParser

import git
import requests
from debian.changelog import Changelog, Version, format_date


p = ArgumentParser(description=__doc__)
p.add_argument('--commit', default=False, action='store_true',
        help=("Commit the changes to the repositories."))
p.add_argument('--directory', default="/tmp/java-to-clojure-migration",
        help='Directory where files and repositories are stored.')
p.add_argument('--force-download', default=False, action='store_true',
        help=("Force download files even if there's already a cached version."))
p.add_argument('--push', default=False, action='store_true',
        help=("Push the changes to the upstream repositories."))
p.add_argument('--quiet', default=False, action='store_true',
        help=("Don't print anything to stdout while running."))
p.add_argument('--skip-clone', default=False, action='store_true',
        help=("Skip cloning the git repositories."))
args = p.parse_args()

AUTHOR = 'Louis-Philippe Véronneau <pollo@debian.org>'

JAVA_PACKAGES = [
    'at-at-clojure', 'cheshire-clojure', 'clj-http-clojure',
    'clj-tuple-clojure', 'clojure-maven-plugin', 'clout-clojure',
    'compojure-clojure', 'fast-zip-clojure', 'hiccup-clojure',
    'honeysql-clojure', 'instaparse-clojure', 'math-combinatorics-clojure',
    'math-numeric-tower-clojure', 'medley-clojure', 'metrics-clojure',
    'pantomime-clojure', 'ring-anti-forgery-clojure', 'ring-defaults-clojure',
    'ring-headers-clojure', 'ring-ssl-clojure', 'specter-clojure',
    'stockpile-clojure', 'tools-namespace-clojure', 'typesafe-config-clojure'
    ]

CLOJURE_TEAM_BASE_URL = 'git@salsa.debian.org:clojure-team/'


def salsa_clone():
    """Clone git repositories from Salsa."""
    if not args.quiet:
        print('Cloning git repositories')
    for package in JAVA_PACKAGES:
        git_url = CLOJURE_TEAM_BASE_URL + package
        clone_path = args.directory + '/' + package
        if args.force_download or not os.path.isdir(clone_path):
            try:
                git.Repo.clone_from(git_url, clone_path)
            except git.exc.GitError:
                if not args.quiet:
                    print('Warning: ' + git_url + " is not a valid git repository")


def apply_changes():
    """Apply the required changes."""
    packages = os.listdir(args.directory)
    for package in packages:
        git_repo = args.directory + '/' + package
        if os.path.isdir(git_repo):
            migrate_team(package, git_repo + '/debian/control')
            update_changelog(git_repo + '/debian/changelog')
            git_operations(git_repo)


def migrate_team(package, control_file):
    """Modify d/control to migrate from the Java to the Clojure Team."""
    with open(control_file, 'r+') as df:
        control = df.read()
        # Change the maintainer
        control = re.sub(
                'Debian Java Maintainers <pkg-java-maintainers@lists.alioth.debian.org>',
                'Debian Clojure Maintainers <team+clojure@tracker.debian.org>',
                control)
        # Change VCS tags
        control = re.sub(
                'Vcs-Git: .+',
                'Vcs-Git: https://salsa.debian.org/clojure-team/' + package + '.git',
                control)
        control = re.sub(
                'Vcs-Browser: .+',
                'Vcs-Browser: https://salsa.debian.org/clojure-team/' + package,
                control)
        df.seek(0)    # Overwrite the file
        df.truncate() #
        df.write(control)


def update_changelog(changelog_file):
    with open(changelog_file, 'r+') as cf:
        ch = Changelog(cf)

        if ch.distributions != 'UNRELEASED':
            ch.new_block(package=ch.package,
                         version=increment_version(ch.version),
                         distributions='UNRELEASED',
                         urgency='medium',
                         author=AUTHOR,
                         date=format_date())
        ch.add_change('')
        ch.add_change('  [ Louis-Philippe Véronneau ]')
        ch.add_change('  * d/control: Migrate to the Clojure Team.')
        ch.add_change('')

        cf.seek(0)    # Overwrite the file
        cf.truncate() #
        ch.write_to_open_file(cf)


def increment_version(v: Version) -> None:
    """Increment a version number.

    For native packages, increment the main version number.
    For other packages, increment the debian revision.

    Args:
        v: Version to increment (modified in place)

    Code comes from the lintian-brush project.
    """
    if v.debian_revision is not None:
        v.debian_revision = re.sub(
            "\\d+$", lambda x: str(int(x.group()) + 1), v.debian_revision
        )
    else:
        v.upstream_version = re.sub(
            "\\d+$", lambda x: str(int(x.group()) + 1), v.upstream_version
        )

    return v


def git_operations(git_repo):
    """Git commit and push operations."""
    repo = git.Repo(git_repo)
    commit_message = 'd/control: Migrate to the Clojure Team.'
    if args.commit:
        repo.git.commit( '-am', commit_message, author=AUTHOR)
    # We verify the HEAD commit is the one we made. Otherwise, we might push
    # something that doesn't need to be pushed.
    if args.push and repo.head.commit.message == commit_message + '\n':
        repo.remotes.origin.push()


def main():
    """Main function."""
    if not args.skip_clone:
        salsa_clone()
    apply_changes()


if __name__ == "__main__":
    main()
