#!/usr/bin/python3

# Copyright 2023, Louis-Philippe Véronneau <pollo@debian.org>
#
# This script is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This script is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this script. If not, see <http://www.gnu.org/licenses/>.

"""
This script is used to compose emails to create bug reports for the asyncore
and asynchat deprecation, using Thunderbird!
"""

import csv
import os
import subprocess


def compose(data):
    """Run the Thunderbird compose commands"""
    subject = f"{data['package']}: deprecation of Python libraries asyncore and asynchat"
    message_path = os.path.join('/tmp/async/', data['package'])
    with open(message_path, "w", encoding="utf-8") as tempfile:
        message = (f"Source: {data['package']}\n"
                 "Severity: important\n"
                 "User: debian-python@lists.debian.org\n"
                 "Usertags: asyncore-asynchat-deprecation\n"
                 "\n"
                 "Dear maintainer(s),\n"
                 "\n"
                 "In Python 3.6, asyncore and asynchat have been formally marked as deprecated. Code that imports these libraries will no longer work from Python 3.12, which is currently in Trixie.\n"
                 "\n"
                 f"Since {data['package']} uses either of these Python libraries, please prepare for this removal and migrate away from them.\n"
                 "\n"
                 "See this link for more details: https://peps.python.org/pep-0594/#deprecated-modules\n"
                 "\n"
                 "Cheers,\n")
        tempfile.write(message)
        subprocess.run(["thunderbird", "-compose", f"to='maintonly@bugs.debian.org',from='pollo@debian.org',subject='{subject}',message={message_path}"])


def main():
    """Main function"""
    with open('asyncore-asynchat-deprecation.csv', newline='') as csvfile:
        csvlist = csv.DictReader(csvfile)
        for data in csvlist:
            compose(data)


if __name__ == "__main__":
    main()
