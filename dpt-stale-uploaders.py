#!/bin/python3

# Copyright 2024, Louis-Philippe Véronneau <pollo@debian.org>
#
# This script is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This script is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this script. If not, see <http://www.gnu.org/licenses/>.

"""
This script is used to get a list of packages in the DPT where people listed as
uploaders have not commited to the git repository in the last 3 years.
"""

import os
import pickle
import re

from argparse import ArgumentParser
from datetime import datetime, timedelta

import debian.deb822
import git
import gitlab

p = ArgumentParser(description=__doc__)
p.add_argument('--clone', default=False, action='store_true',
        help="Skip cloning the git repositories.")
p.add_argument('--force-download', default=False, action='store_true',
        help="Force download repository even if there's already a cached version.")
p.add_argument('--cpath', required=True,
        help="Path to the directory where to clone repositories.")
p.add_argument('--private-token', required=True,
        help="Gitlab private token.")
p.add_argument('--repo-list', help="Path to the Pickle file containing the "
                                    "gitlab projects object.")
args = p.parse_args()

GROUPID=9360


def write_repo_list(data):
    """Write repository list to disk"""
    repo_list_file = os.path.join(os.getcwd(), 'dpt-repo-list.pickle')
    with open(repo_list_file, 'wb') as _repo:
        pickle.dump(data, _repo)


def get_repo_list():
    """Get the repository list from the disk"""
    repo_list = []
    repo_list_file = os.path.join(os.getcwd(), 'dpt-repo-list.pickle')
    with open(repo_list_file, 'rb') as _repo:
        repo_list = pickle.load(_repo)
    return repo_list


def salsa_get_projects(repo_list=None):
    """Get list of repositories from Salsa"""
    salsa = gitlab.Gitlab('https://salsa.debian.org/', args.private_token)
    salsa.enable_debug()
    group = salsa.groups.get(GROUPID)
    repo_list = group.projects.list(all=True)
    return repo_list


def salsa_clone(repo_list, date, cpath):
    """Clone git repositories from Salsa, with a shallow-clone since a date."""
    len_repo_list = len(repo_list)
    for idx, project in enumerate(repo_list):
        clone_path = os.path.join(cpath, project.path)
        if args.force_download or not os.path.isdir(clone_path):
            print(f"Cloning {project.path} [{idx+1}/{len_repo_list}]")
            try:
                git.Repo.clone_from(url=project.http_url_to_repo,
                                    to_path=clone_path,
                                    multi_options=[f"--shallow-since={date.timestamp()}"])
            except git.exc.GitCommandError:
                git.Repo.clone_from(url=project.http_url_to_repo,
                                    to_path=clone_path)

def check_git_empty(repo):
    """Check if a git repository is empty"""
    empty = False
    nobjects = len(repo.git.rev_list('--objects', '--all'))
    if nobjects == 0:
        empty = True
    return empty


def find_git_author(repo_path, date):
    """List all the authors who commited since a date for a repository"""
    repo = git.Repo(repo_path)
    branch_names = ['debian/latest', 'debian/main', 'debian/master',
                    'debian/unstable', 'debian', 'main', 'master']
    authors = []
    for idx, branch in enumerate(branch_names):
        try:
            repo.git.checkout(branch)
            break
        except git.exc.GitCommandError:
            # raise only if we're at the end of our branch_names list
            if idx+1 == len(branch_names):
                empty = check_git_empty(repo)
                if empty:
                    print(f"{repo_path} is an empty git directory")
                else:
                    bnames = ', '.join(map(str, branch_names))
                    print(f"No matching branches found in [{bnames}] for project {repo_path}")
                return authors
    commits = list(repo.iter_commits(since=date))
    for commit in commits:
        authors.append(commit.author.name)
    return authors


def format_uploaders(uploaders):
    """Format the deb822 Uploaders field into a list"""
    replacements = {"\n": "",      # newlines
                    ", +": "\n",   # break multiple lines
                    " <.*?>": "",  # emails
                    "\t": "",      # tabs
                    ",$": "",      # trailing comma
                    "^ ": "",      # first space
                    }
    for pattern, repl in replacements.items():
        uploaders = re.sub(pattern, repl, uploaders)
    uploaders_list = uploaders.splitlines()
    return uploaders_list


def diff_authors(repo_list, date, cpath):
    """Find if the package's Uploaders have commited since X."""
    stale_dict = {}
    for project in repo_list:
        repo_path = os.path.join(cpath, project.path)
        authors = find_git_author(repo_path, date)
        debian_control = os.path.join(repo_path, 'debian/control')
        if os.path.isfile(debian_control):
            with open(debian_control, 'r', encoding="utf-8") as _control:
                control = debian.deb822.Deb822(_control)
                uploaders = control.get('Uploaders')
                if uploaders is None:
                    uploaders = 'None'
        else:
            uploaders = ''
            print(f"{project.path} does not contain a 'debian/control' file")
        uploaders_list = format_uploaders(uploaders)
        for uploader in uploaders_list:
            if uploader not in authors:
                if project.path in stale_dict:
                    stale_dict[project.path].append(uploader)
                else:
                    stale_dict[project.path] = [uploader]
    return stale_dict


def main():
    """Main function."""
    if args.repo_list:
        repo_list = get_repo_list()
    else:
        repo_list = salsa_get_projects()
        write_repo_list(repo_list)
    date = datetime.now() - timedelta(days=3*365)
    if args.clone:
        salsa_clone(repo_list, date, args.cpath)
    stale_dict = diff_authors(repo_list, date, args.cpath)
    for key, value in sorted(stale_dict.items()):
        print(f"{key}: {', '.join(value)}")


if __name__ == "__main__":
    main()
