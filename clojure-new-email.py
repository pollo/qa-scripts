#!/bin/python3

# Copyright 2021, Louis-Philippe Véronneau <pollo@debian.org>
#
# This script is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This script is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this script. If not, see <http://www.gnu.org/licenses/>.

"""
This script is used to replace the old Clojure Team maintainer contact email by
the new one.
"""

import json
import os
import re

from argparse import ArgumentParser

import git
import requests
from debian.changelog import Changelog, Version, format_date


p = ArgumentParser(description=__doc__)
p.add_argument('--commit', default=False, action='store_true',
        help=("Commit the changes to the repositories."))
p.add_argument('--directory', default="/tmp/clojure-new-email",
        help='Directory where files and repositories are stored.')
p.add_argument('--force-download', default=False, action='store_true',
        help=("Force download files even if there's already a cached version."))
p.add_argument('--push', default=False, action='store_true',
        help=("Push the changes to the upstream repositories."))
p.add_argument('--quiet', default=False, action='store_true',
        help=("Don't print anything to stdout while running."))
p.add_argument('--skip-clone', default=False, action='store_true',
        help=("Skip cloning the git repositories."))
args = p.parse_args()

AUTHOR = 'Louis-Philippe Véronneau <pollo@debian.org>'


def get_lintian_data():
    """Download and locally cache the lintian JSON file."""
    lintian_url = "https://lintian.debian.org/query/contexts?tag=mail-contact"
    data_path = args.directory + '/lintian.json'
    data_cached = os.path.isfile(data_path)

    if not os.path.isdir(args.directory):
        os.makedirs(args.directory)

    if (data_cached and not args.force_download):
        print("Using cached lintian data")
        f = open(data_path, 'r')
    elif (args.force_download or not data_cached):
        print("Downloading lintian data")
        r = requests.get(lintian_url, allow_redirects=True)
        f = open(data_path, 'wb+')
        f.write(r.content)
        f.flush()
        f.seek(0)
    lintian_data = json.load(f)

    return lintian_data, f


def find_source_package(lintian_data):
    """Extract the list of source packages from the raw Lintian data."""
    package_list = []
    for package in lintian_data:
        if 'pkg-clojure-maintainers@lists.alioth.debian.org' in package["note"]:
            package_list.append(package["source_name"])
    package_list = list(set(package_list))  # Remove duplicates

    return package_list


def salsa_clone(package_list):
    """Clone git repositories from Salsa."""
    if not args.quiet:
        print('Cloning git repositories')
    clojure_team_base_url = 'git@salsa.debian.org:clojure-team/'
    for package in package_list:
        git_url = clojure_team_base_url + package
        clone_path = args.directory + '/' + package
        if args.force_download or not os.path.isdir(clone_path):
            try:
                git.Repo.clone_from(git_url, clone_path)
            except git.exc.GitError:
                if not args.quiet:
                    print('Warning: ' + git_url + " is not a valid git repository")


def apply_changes():
    """Apply the required changes."""
    packages = os.listdir(args.directory)
    for package in packages:
        git_repo = args.directory + '/' + package
        if os.path.isdir(git_repo):
            nchanges = change_email(git_repo + '/debian/control')
            if nchanges > 0:
                update_changelog(git_repo + '/debian/changelog')
                git_operations(git_repo)


def change_email(control_file):
    old_email = 'pkg-clojure-maintainers@lists.alioth.debian.org'
    new_email = 'team+clojure@tracker.debian.org'
    with open(control_file, 'r+') as df:
        control = df.read()
        control, nchanges = re.subn(old_email, new_email, control)
        if nchanges > 0:
            df.seek(0)    # Overwrite the file
            df.truncate() #
            df.write(control)
    return nchanges


def update_changelog(changelog_file):
    with open(changelog_file, 'r+') as cf:
        ch = Changelog(cf)

        if ch.distributions != 'UNRELEASED':
            ch.new_block(package=ch.package,
                         version=increment_version(ch.version),
                         distributions='UNRELEASED',
                         urgency='medium',
                         author=AUTHOR,
                         date=format_date())
        ch.add_change('')
        ch.add_change('  [ Louis-Philippe Véronneau ]')
        ch.add_change('  * d/control: New email for the Clojure Team.')
        ch.add_change('')

        cf.seek(0)    # Overwrite the file
        cf.truncate() #
        ch.write_to_open_file(cf)


def increment_version(v: Version) -> None:
    """Increment a version number.

    For native packages, increment the main version number.
    For other packages, increment the debian revision.

    Args:
        v: Version to increment (modified in place)

    Code comes from the lintian-brush project.
    """
    if v.debian_revision is not None:
        v.debian_revision = re.sub(
            "\\d+$", lambda x: str(int(x.group()) + 1), v.debian_revision
        )
    else:
        v.upstream_version = re.sub(
            "\\d+$", lambda x: str(int(x.group()) + 1), v.upstream_version
        )

    return v


def git_operations(git_repo):
    """Git commit and push operations."""
    repo = git.Repo(git_repo)
    commit_message = 'Replace the old Clojure Team email by the new one.'
    if args.commit:
        repo.git.commit( '-am', commit_message, author=AUTHOR)
    # We verify the HEAD commit is the one we made. Otherwise, we might push
    # something that doesn't need to be pushed.
    if args.push and repo.head.commit.message == commit_message + '\n':
        repo.remotes.origin.push()


def main():
    """Main function."""
    lintian_data, f = get_lintian_data()
    package_list = find_source_package(lintian_data)
    if not args.skip_clone:
        salsa_clone(package_list)
    apply_changes()
    f.close()


if __name__ == "__main__":
    main()
