#!/usr/bin/python3

# Copyright 2024, Louis-Philippe Véronneau <pollo@debian.org>
#
# This script is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This script is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this script. If not, see <http://www.gnu.org/licenses/>.

"""
This script is used to compose emails to create bug reports for the PEP 594
standard libraries deprecation for Python 3.13, using Thunderbird!

UDD command to generate the CSV data:

c udd;
copy (select * from lintian_results where tag='uses-deprecated-python-stdlib') To STDOUT WITH (DELIMITER ',', FORMAT CSV, HEADER) \g '/home/pollo/pep-594-py313.csv';
"""

import csv
import os
import subprocess


def build_dict(line, data):
    """Build the dictionnary that will be used to generate the emails"""
    library = line['information'].split(' ', 1)[0]
    code = line['information'].split(' ')[-1]
    code = code.strip('[').rstrip(']\n')
    # Too many false-positive for now with uu
    if library != 'uu' and 'Python 3.13' in line['information']:
        tag_dict = {'library': library, 'code': code }
        # We're building a dictionary where keys are source package names, and
        # values a list of dictionaries
        if line['source'] not in data:
            data[line['source']] = [tag_dict]
        else:
            data[line['source']].append(tag_dict)
    return data


def compose(source, data):
    """Run the Thunderbird compose commands"""
    subject = f"{source}: removal of Python standard libraries in Python 3.13"
    message_path = os.path.join('/tmp/stdlib313/', source)
    lib_code_block = ''
    for item in data[source]:
        lib_code_block = lib_code_block + item['library'] + ': ' + item['code'] + '\n'
    with open(message_path, "w", encoding="utf-8") as tempfile:
        message = (f"Source: {source}\n"
                 "Severity: important\n"
                 "User: debian-python@lists.debian.org\n"
                 "Usertags: pep-594-deprecation-313\n"
                 "\n"
                 "Dear maintainer(s),\n"
                 "\n"
                 "Python 3.13 removes a large amount of so called 'dead battery' libraries from the standard library. As such, code that imports these libraries will no longer work in Python 3.13, which is the targeted version for Trixie.\n"
                 "\n"
                 "The following removed libraries were found in this package:\n"
                 "\n"
                 f"{lib_code_block}"
                 "\n"
                 "See this link for more details: https://peps.python.org/pep-0594/#deprecated-modules\n"
                 "\n"
                 "Cheers,\n")
        tempfile.write(message)
        subprocess.run(["thunderbird", "-compose", f"to='maintonly@bugs.debian.org',from='pollo@debian.org',subject='{subject}',message={message_path}"])


def dd_list(data):
    """Create a dd-list.txt file"""
    sources = ' '.join(data.keys())
    results = subprocess.run(["dd-list", "-i"], input=sources.encode())
    dd_list_txt = results.stdout
    return dd_list_txt


def main():
    """Main function"""
    with open('pep-594-py313.csv', newline='') as csvfile:
        csvlist = csv.DictReader(csvfile)
        data = {}
        for line in csvlist:
            data = build_dict(line, data)
        dd_list_txt = dd_list(data)
        counter = 0
        for source in data:
            compose(source, data)
            counter += 1
            if counter % 10 == 0:
                while True:
                    prompt = input('press y to continue\n')
                    if prompt == 'y':
                        break
                    else:
                        print ("\n")


if __name__ == "__main__":
    main()
