#!/usr/bin/python3

# Copyright 2022, Louis-Philippe Véronneau <pollo@debian.org>
#
# This script is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This script is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this script. If not, see <http://www.gnu.org/licenses/>.

"""
This script is used to compose emails to create bug reports for the pytest 6 to
7 migration, for failing autopkgtests, using Thunderbird!
"""

import csv
import os
import subprocess


def compose(data):
    """Run the Thunderbird compose commands"""
    subject = f"{data['package']}: autopkgtest regression on {data['archs']}: pytest7 migration - {data['error_msg']}"
    message_path = os.path.join('/tmp/pytest7/', data['package'])
    with open(message_path, "w", encoding="utf-8") as tempfile:
        message = (f"Source: {data['package']}\n"
                 f"Version: {data['version']}\n"
                 "Severity: serious\n"
                 "X-Debbugs-CC: debian-ci@lists.debian.org\n"
                 "User: debian-python@lists.debian.org\n"
                 "Usertags: pytest7\n"
                 "User: debian-ci@lists.debian.org\n"
                 "Usertags: regression\n"
                 "Control: affects -1 src:pytest\n"
                 "\n"
                 "Dear maintainer(s),\n"
                 "\n"
                 "The latest version of pytest in unstable (7.1.2-2) seems to be breaking the autopkgtests for this package. This regression currently keeps pytest from migrating to testing.\n"
                 "\n"
                 f"You can find the CI logs here: https://ci.debian.net/packages/d/{data['package']}/\n"
                 "\n"
                 f"I had a closer look at the error logs and identified the regression as: '{data['error_msg']}'. Hopefully, that's helpful!\n"
                 "\n"
                 "If you fail to reproduce this, please provide a build log and diff it with the one provided so that we can identify if something relevant changed in the meantime.\n")
        tempfile.write(message)
        subprocess.run(["thunderbird", "-compose", f"to='submit@bugs.debian.org',from='pollo@debian.org',subject='{subject}',message={message_path}"])


def main():
    """Main function"""
    with open('pytest-7-ftbfs-autopkgtest.csv', newline='') as csvfile:
        csvlist = csv.DictReader(csvfile)
        for data in csvlist:
            compose(data)


if __name__ == "__main__":
    main()
