#!/usr/bin/python3

# Copyright 2022, Louis-Philippe Véronneau <pollo@debian.org>
#
# This script is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This script is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this script. If not, see <http://www.gnu.org/licenses/>.

"""
This script is used to compose emails to create bug reports for the pytest 6 to
7 migration, using Thunderbird!
"""

import csv
import os
import subprocess


def qa_url(data):
    """Outputs the QA URL log"""
    url_base = 'http://qa-logs.debian.net/2022/06/09/pytest/'
    data['url'] = url_base + data['package'] + '_' + data['version'] + '_' + 'unstable_pytest-exp.log'
    return data


def compose(data):
    """Run the Thunderbird compose commands"""
    subject = f"{data['package']}: FTBFS: pytest7 regression, {data['error_msg']}"
    message_path = os.path.join('/tmp/pytest7/', data['package'])
    with open(message_path, "w", encoding="utf-8") as tempfile:
        message = (f"Source: {data['package']}\n"
                 f"Version: {data['version']}\n"
                 "Severity: serious\n"
                 "Justification: FTBFS\n"
                 "Tags: bookworm sid ftbfs\n"
                 "User: debian-python@lists.debian.org\n"
                 "Usertags: pytest7\n"
                 "\n"
                 "Hi,\n"
                 "\n"
                 "During a selected rebuild of python packages in sid, your package failed to build with pytest version 7.1.2-1.\n"
                 "\n"
                 f"The full build log is available from: {data['url']}\n"
                 "\n"
                 f"I had a closer look at the error log and identified the regression as: '{data['error_msg']}'. Hopefully, that's helpful!\n"
                 "\n"
                 "All bugs filed during this rebuild are listed at:\n"
                 "https://bugs.debian.org/cgi-bin/pkgreport.cgi?tag=pytest7;users=debian-python@lists.debian.org\n"
                 "or:\n"
                 "https://udd.debian.org/bugs/?release=na&merged=ign&fnewerval=7&flastmodval=7&fusertag=only&fusertagtag=pytest7&fusertaguser=debian-python@lists.debian.org&allbugs=1&cseverity=1&ctags=1&caffected=1#results\n"
                 "\n"
                 "If you reassign this bug to another package, please mark it as 'affects'-ing this package. See https://www.debian.org/Bugs/server-control#affects\n"
                 "\n"
                 "If you fail to reproduce this, please provide a build log and diff it with the one provided so that we can identify if something relevant changed in the meantime.\n")
        tempfile.write(message)
        subprocess.run(["thunderbird", "-compose", f"to='submit@bugs.debian.org',from='pollo@debian.org',subject='{subject}',message={message_path}"])


def main():
    """Main function"""
    with open('pytest-7-ftbfs.csv', newline='') as csvfile:
        csvlist = csv.DictReader(csvfile)
        for data in csvlist:
            data = qa_url(data)
            compose(data)


if __name__ == "__main__":
    main()
